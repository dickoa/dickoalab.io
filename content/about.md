---
title: About me
comments: false
---

I am a statistical consultant and R enthusiast.
I believe in the use of data science for social good and have worked with several non-profit and research organizations.
