---
output: hugodown::md_document

title: "How to get arabic names for Mauritania Moughataa"
author: "Ahmadou Dicko"
date: "2018-10-01"
tags:
  - hdx
draft: false
rmd_hash: e15b31029af1652c

---

On HDX, you can download and use the administrative boundaries of Mauritania but with one caveat the names of the different administrative divisions are translated from Arabic to English. For some analysis, it can be useful to have also the Arabic name in the same table. In this post, we are going to scrape a table from with the Arabic name from a website before joining this table to our administrative boundaries data. We will need the [`rhdx`](https://gitlab.com/dickoa/rhdx) package (not yet on CRAN) and the following packages:

We can use [`rhdx::pull_dataset`](https://rdrr.io/pkg/rhdx/man/pull_dataset.html) to read the [Mauritania administrative boundaries](https://data.humdata.org/dataset/mauritania-administrative-boundaries) dataset in R and use [`rhdx::get_resources`](https://rdrr.io/pkg/rhdx/man/get_resources.html) to list available resources (aka files).

We can see from the output that the 4th resource contains the second administrative level known as Moughataa.

We can see that the Arabic names are not available in this data, we can even visualize the available name using `ggplot2` and `sf`.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>mrt_adm2</span> <span class='o'>%&gt;%</span>
  <span class='nf'>ggplot</span>() <span class='o'>+</span>
  <span class='nf'>geom_sf</span>() <span class='o'>+</span>
  <span class='nf'>geom_sf_label</span>(<span class='nf'>aes</span>(label = <span class='k'>admin2Name</span>)) <span class='o'>+</span>
  <span class='nf'>theme_minimal</span>()
<span class='c'>#&gt; Warning in st_point_on_surface.sfc(sf::st_zm(x)): st_point_on_surface may not give correct results for longitude/latitude data</span>
</code></pre>
<img src="figs/unnamed-chunk-4-1.png" width="700px" style="display: block; margin: auto;" />

</div>

We need the Arabic name and the [City Population website](https://www.citypopulation.de) provides a lot of information on population and native name for administrative areas. You can check the Mauritania page [here](https://www.citypopulation.de/php/mauritania-admin.php). The page are dynamic and in order to scrape and get the information we need, we have to mimic web browser session using an appropriate [user agent](https://www.howtogeek.com/114937/htg-explains-whats-a-browser-user-agent/). Using the right user agent, we can the `rvest` R package to scrape the data.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>uastring</span> <span class='o'>&lt;-</span> <span class='s'>"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"</span>
<span class='k'>url</span> <span class='o'>&lt;-</span> <span class='s'>"https://www.citypopulation.de/php/mauritania-admin.php"</span>

<span class='k'>url</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rvest.tidyverse.org/reference/html_session.html'>html_session</a></span>(<span class='nf'><a href='https://httr.r-lib.org/reference/user_agent.html'>user_agent</a></span>(<span class='k'>uastring</span>)) <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rvest.tidyverse.org/reference/html_nodes.html'>html_nodes</a></span>(<span class='s'>"table#tl.data"</span>) <span class='o'>%&gt;%</span> <span class='c'>## extract the table</span>
  <span class='nf'><a href='https://rvest.tidyverse.org/reference/html_table.html'>html_table</a></span>() <span class='o'>%&gt;%</span> <span class='c'>## turn it into data in R</span>
  <span class='nf'>first</span>() <span class='o'>%&gt;%</span> <span class='c'>## extract the list with the table inside</span>
  <span class='nf'>select</span>(admin2NameWeb = <span class='k'>Name</span>, admin2NameNative = <span class='k'>Native</span>, status = <span class='k'>Status</span>) <span class='o'>%&gt;%</span> <span class='c'>## rename column</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span>(<span class='k'>status</span> <span class='o'>%in%</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span>(<span class='s'>"Department"</span>, <span class='s'>"Capital District"</span>)) <span class='o'>-&gt;</span> <span class='k'>arabic_adm2</span> <span class='c'>## filter by departement (adm2) or capital district for Nouackshott</span>
<span class='nf'>glimpse</span>(<span class='k'>arabic_adm2</span>)
<span class='c'>#&gt; Rows: 47</span>
<span class='c'>#&gt; Columns: 3</span>
<span class='c'>#&gt; $ admin2NameWeb    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Aïoun El Atrouss", "Akjoujt", "Aleg"…</span></span>
<span class='c'>#&gt; $ admin2NameNative <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "لعيون", "اكجوجت", "الاك", "امرج", "أ…</span></span>
<span class='c'>#&gt; $ status           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Department", "Department", "Departme…</span></span></code></pre>

</div>

As you can see, this table contains some Arabic names (`admin2NameNative`), we now need to join it to our boundaries data. However, because of spelling differences between the two `admin2Name` columns in each table, we need to apply some approximative matching ([`stringdist::amatch`](https://rdrr.io/pkg/stringdist/man/amatch.html)).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>ind</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/stringdist/man/amatch.html'>amatch</a></span>(<span class='k'>arabic_adm2</span><span class='o'>$</span><span class='k'>admin2NameWeb</span>, <span class='k'>mrt_adm2</span><span class='o'>$</span><span class='k'>admin2Name</span>, maxDist = <span class='m'>5</span>)
<span class='k'>arabic_adm2</span><span class='o'>$</span><span class='k'>admin2Name</span> <span class='o'>&lt;-</span> <span class='k'>mrt_adm2</span><span class='o'>$</span><span class='k'>admin2Name</span>[<span class='k'>ind</span>]

<span class='k'>non_matched_admin2</span> <span class='o'>&lt;-</span> <span class='nf'>anti_join</span>(<span class='k'>mrt_adm2</span>, <span class='k'>arabic_adm2</span>)<span class='o'>$</span><span class='k'>admin2Name</span>
<span class='k'>non_matched_admin2</span>
<span class='c'>#&gt; [1] "Aioun"</span></code></pre>

</div>

We are missing Aioun but since we have most of the available Moughataa, we can join the two data and check the final results in a map.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>final</span> <span class='o'>&lt;-</span> <span class='nf'>left_join</span>(<span class='k'>mrt_adm2</span>, <span class='nf'>select</span>(<span class='k'>arabic_adm2</span>, <span class='o'>-</span><span class='k'>status</span>, <span class='o'>-</span><span class='k'>admin2NameWeb</span>))

<span class='nf'>ggplot</span>(<span class='k'>final</span>) <span class='o'>+</span>
  <span class='nf'>geom_sf</span>() <span class='o'>+</span>
  <span class='nf'>geom_sf_label</span>(<span class='nf'>aes</span>(label = <span class='k'>admin2NameNative</span>)) <span class='o'>+</span>
  <span class='nf'>theme_minimal</span>()
</code></pre>
<img src="figs/unnamed-chunk-7-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Session info for this analysis.

<details>

<summary>Session info</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>devtools</span>::<span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/session_info.html'>session_info</a></span>()
<span class='c'>#&gt; ─ Session info ────────────────────────────────────────────────</span>
<span class='c'>#&gt;  setting  value                                      </span>
<span class='c'>#&gt;  version  R version 4.0.2 Patched (2020-06-22 r78732)</span>
<span class='c'>#&gt;  os       Arch Linux                                 </span>
<span class='c'>#&gt;  system   x86_64, linux-gnu                          </span>
<span class='c'>#&gt;  ui       X11                                        </span>
<span class='c'>#&gt;  language (EN)                                       </span>
<span class='c'>#&gt;  collate  en_US.UTF-8                                </span>
<span class='c'>#&gt;  ctype    en_US.UTF-8                                </span>
<span class='c'>#&gt;  tz       Africa/Abidjan                             </span>
<span class='c'>#&gt;  date     2020-06-24                                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; ─ Packages ────────────────────────────────────────────────────</span>
<span class='c'>#&gt;  package     * version    date       lib</span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1]</span>
<span class='c'>#&gt;  backports     1.1.8      2020-06-17 [1]</span>
<span class='c'>#&gt;  base64enc     0.1-3      2015-07-28 [1]</span>
<span class='c'>#&gt;  blob          1.2.1      2020-01-20 [1]</span>
<span class='c'>#&gt;  broom         0.5.6      2020-04-20 [1]</span>
<span class='c'>#&gt;  callr         3.4.3      2020-03-28 [1]</span>
<span class='c'>#&gt;  cellranger    1.1.0      2016-07-27 [1]</span>
<span class='c'>#&gt;  class         7.3-17     2020-04-26 [1]</span>
<span class='c'>#&gt;  classInt      0.4-3      2020-04-07 [1]</span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1]</span>
<span class='c'>#&gt;  codetools     0.2-16     2018-12-24 [1]</span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1]</span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1]</span>
<span class='c'>#&gt;  crul          0.9.2.92   2020-06-17 [1]</span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1]</span>
<span class='c'>#&gt;  DBI           1.1.0      2019-12-15 [1]</span>
<span class='c'>#&gt;  dbplyr        1.4.4      2020-05-27 [1]</span>
<span class='c'>#&gt;  desc          1.2.0      2018-05-01 [1]</span>
<span class='c'>#&gt;  devtools      2.3.0      2020-04-10 [1]</span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1]</span>
<span class='c'>#&gt;  downlit       0.0.0.9000 2020-06-24 [1]</span>
<span class='c'>#&gt;  dplyr       * 1.0.0      2020-05-29 [1]</span>
<span class='c'>#&gt;  e1071         1.7-3      2019-11-26 [1]</span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1]</span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1]</span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1]</span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1]</span>
<span class='c'>#&gt;  forcats     * 0.5.0      2020-03-01 [1]</span>
<span class='c'>#&gt;  fs            1.4.1      2020-04-04 [1]</span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1]</span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1]</span>
<span class='c'>#&gt;  glue          1.4.1      2020-05-13 [1]</span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1]</span>
<span class='c'>#&gt;  haven         2.3.1      2020-06-01 [1]</span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1]</span>
<span class='c'>#&gt;  hoardr        0.5.2.93   2020-06-08 [1]</span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1]</span>
<span class='c'>#&gt;  httpcode      0.3.0      2020-04-10 [1]</span>
<span class='c'>#&gt;  httr        * 1.4.1      2019-08-05 [1]</span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-06-24 [1]</span>
<span class='c'>#&gt;  jsonlite      1.6.1      2020-02-02 [1]</span>
<span class='c'>#&gt;  KernSmooth    2.23-17    2020-04-26 [1]</span>
<span class='c'>#&gt;  knitr         1.29       2020-06-23 [1]</span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1]</span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1]</span>
<span class='c'>#&gt;  lubridate     1.7.9      2020-06-08 [1]</span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1]</span>
<span class='c'>#&gt;  memoise       1.1.0      2017-04-21 [1]</span>
<span class='c'>#&gt;  modelr        0.1.8      2020-05-19 [1]</span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1]</span>
<span class='c'>#&gt;  nlme          3.1-148    2020-05-24 [1]</span>
<span class='c'>#&gt;  pillar        1.4.4      2020-05-05 [1]</span>
<span class='c'>#&gt;  pkgbuild      1.0.8      2020-05-07 [1]</span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1]</span>
<span class='c'>#&gt;  pkgload       1.1.0      2020-05-29 [1]</span>
<span class='c'>#&gt;  prettyunits   1.1.1      2020-01-24 [1]</span>
<span class='c'>#&gt;  processx      3.4.2      2020-02-09 [1]</span>
<span class='c'>#&gt;  ps            1.3.3      2020-05-08 [1]</span>
<span class='c'>#&gt;  purrr       * 0.3.4      2020-04-17 [1]</span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1]</span>
<span class='c'>#&gt;  rappdirs      0.3.1      2016-03-28 [1]</span>
<span class='c'>#&gt;  Rcpp          1.0.4.12   2020-06-16 [1]</span>
<span class='c'>#&gt;  readr       * 1.3.1      2018-12-21 [1]</span>
<span class='c'>#&gt;  readxl        1.3.1      2019-03-13 [1]</span>
<span class='c'>#&gt;  remotes       2.1.1      2020-02-15 [1]</span>
<span class='c'>#&gt;  reprex        0.3.0      2019-05-16 [1]</span>
<span class='c'>#&gt;  rhdx        * 0.1.0.9000 2020-06-14 [1]</span>
<span class='c'>#&gt;  rlang         0.4.6      2020-05-02 [1]</span>
<span class='c'>#&gt;  rmarkdown     2.3.1      2020-06-24 [1]</span>
<span class='c'>#&gt;  rprojroot     1.3-2      2018-01-03 [1]</span>
<span class='c'>#&gt;  rstudioapi    0.11       2020-02-07 [1]</span>
<span class='c'>#&gt;  rvest       * 0.3.5      2019-11-08 [1]</span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1]</span>
<span class='c'>#&gt;  selectr       0.4-2      2019-11-20 [1]</span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1]</span>
<span class='c'>#&gt;  sf          * 0.9-4      2020-06-19 [1]</span>
<span class='c'>#&gt;  stringdist  * 0.9.5.5    2019-10-21 [1]</span>
<span class='c'>#&gt;  stringi       1.4.6      2020-02-17 [1]</span>
<span class='c'>#&gt;  stringr     * 1.4.0      2019-02-10 [1]</span>
<span class='c'>#&gt;  testthat      2.3.2      2020-03-02 [1]</span>
<span class='c'>#&gt;  tibble      * 3.0.1      2020-04-20 [1]</span>
<span class='c'>#&gt;  tidyr       * 1.1.0      2020-05-20 [1]</span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1]</span>
<span class='c'>#&gt;  tidyverse   * 1.3.0      2019-11-21 [1]</span>
<span class='c'>#&gt;  triebeard     0.3.0      2016-08-04 [1]</span>
<span class='c'>#&gt;  units         0.6-7      2020-06-13 [1]</span>
<span class='c'>#&gt;  urltools      1.7.3      2019-04-14 [1]</span>
<span class='c'>#&gt;  usethis       1.6.1      2020-04-29 [1]</span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1]</span>
<span class='c'>#&gt;  vctrs         0.3.1      2020-06-05 [1]</span>
<span class='c'>#&gt;  withr         2.2.0      2020-04-20 [1]</span>
<span class='c'>#&gt;  xfun          0.15       2020-06-21 [1]</span>
<span class='c'>#&gt;  xml2        * 1.3.2      2020-04-23 [1]</span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1]</span>
<span class='c'>#&gt;  source                            </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-lib/downlit@9191e1f)    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-lib/hugodown@f7df565)   </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (rstudio/rmarkdown@b53a85a)</span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-spatial/sf@0b08ed5)     </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /usr/lib/R/library</span></code></pre>

</div>

</details>

