---
output: hugodown::md_document

title: "Fatal events in the Sahel"
author: "Ahmadou Dicko"
date: "2019-02-27"

tags:
  - rhdx
rmd_hash: 6b9eb4f74ff87775

---

In this short post, we will show how to use the `rhdx`, `dplyr`, `purrr`, `sf` and `gganimate` R packages to show the number of fatal incidents in 5 Sahelian countries. The [`rhdx`](https://gitlab.com/dickoa/rhdx) package is not yet on CRAN, so you will need to use the [`remotes`](https://cran.r-project.org/web/packages/remotes/index.html) package to install it first:

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>remotes</span>::<span class='nf'><a href='https://remotes.r-lib.org/reference/install_gitlab.html'>install_gitlab</a></span>(<span class='s'>"dickoa/rhdx"</span>) <span class='c'>## github mirror also avalailable</span></code></pre>

</div>

`gganimate` depends on the [`gifski`](https://cran.r-project.org/package=gifski) R package and to install it, make sure you first have [the `gifski` Rust cargo crate](https://gif.ski/) installed on your system.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/utils/install.packages.html'>install.packages</a></span>(<span class='s'>"gifski"</span>)</code></pre>

</div>

This analysis was inspired by this tweet by José Luengo-Cabrera, researcher at the Crisis Group.

<blockquote class="twitter-tweet" data-lang="en">
<p lang="en" dir="ltr">
G5 Sahel: conflict-related fatalities totaled 2,832 in 2018, a 74% increase relative to 2017.<br><br>- On average, 63% of fatalities have been concentrated in Mali since 2012.<br>- Last year, fatalities were largely located in central Mali, the Liptako-Gourma region & the Lake Chad basin. <a href="https://t.co/feRtcxsScb">pic.twitter.com/feRtcxsScb</a>
</p>
--- José Luengo-Cabrera (@J\_LuengoCabrera) <a href="https://twitter.com/J_LuengoCabrera/status/1100340244535263232?ref_src=twsrc%5Etfw">February 26, 2019</a>
</blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Our visualization will be done for the following countries in the Sahel : *Burkina Faso*, *Mali*, *Chad*, *Mauritania* and *Niger*. There is a lot of insecurity and conflicts in these countries that resulted in the death of several thousands of people.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/base/library.html'>library</a></span>(<span class='k'><a href='http://tidyverse.tidyverse.org'>tidyverse</a></span>)
<span class='nf'><a href='https://rdrr.io/r/base/library.html'>library</a></span>(<span class='k'><a href='https://r-spatial.github.io/sf'>sf</a></span>)
<span class='nf'><a href='https://rdrr.io/r/base/library.html'>library</a></span>(<span class='k'><a href='https://gitlab.com/dickoa/rhdx'>rhdx</a></span>)
<span class='nf'><a href='https://rdrr.io/r/base/library.html'>library</a></span>(<span class='k'><a href='https://gganimate.com'>gganimate</a></span>)</code></pre>

</div>

The goal of this post is to visualize the number of fatal events in theses countries between 2012 and 2018 using an animated map. In order to do that, will need to get the administrative boundaries for these countries. We can get the latest boundaries validated by the governments and the humanitarian community directly from [*HDX*](https://data.humdata.org/) using the `rhdx` package. We will use [`rhdx::pull_dataset`](https://rdrr.io/pkg/rhdx/man/pull_dataset.html) function and use the name of the dataset of interest as value. [`rhdx::get_resource`](https://rdrr.io/pkg/rhdx/man/get_resource.html) and [`rhdx::download_resource`](https://rdrr.io/pkg/rhdx/man/download_resource.html) allow to respectively get the a resource by its index and read the data into memory.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/set_rhdx_config.html'>set_rhdx_config</a></span>()

<span class='k'>wca</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/pull_dataset.html'>pull_dataset</a></span>(<span class='s'>"west-and-central-africa-administrative-boundaries-levels"</span>) <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/get_resource.html'>get_resource</a></span>(<span class='m'>1</span>) <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/read_resource.html'>read_resource</a></span>()
<span class='c'>#&gt; reading layer: wca_adm0</span>
<span class='nf'>glimpse</span>(<span class='k'>wca</span>)
<span class='c'>#&gt; Rows: 24</span>
<span class='c'>#&gt; Columns: 6</span>
<span class='c'>#&gt; $ OBJECTID   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, …</span></span>
<span class='c'>#&gt; $ admin0Name <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Benin", "Burkina Faso", "Cabo Verde", "Cam…</span></span>
<span class='c'>#&gt; $ admin0Pcod <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "BJ", "BF", "CV", "CM", "CF", "TD", "CI", "…</span></span>
<span class='c'>#&gt; $ Shape_Leng <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 19.006363, 31.466913, 13.599357, 47.469736,…</span></span>
<span class='c'>#&gt; $ Shape_Area <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 9.52157812, 22.72097221, 0.34276153, 38.086…</span></span>
<span class='c'>#&gt; $ geometry   <span style='color: #555555;font-style: italic;'>&lt;MULTIPOLYGON [°]&gt;</span><span> MULTIPOLYGON (((2.886863 12...…</span></span></code></pre>

</div>

`wca` is a [`Simple Feature`](https://r-spatial.github.io/sf/articles/sf1.html) and we can manipulate it using `sf` and `dplyr`. The data downloaded from *HDX* covers the 24 countries of West and Central Africa, we will filter the data to extract the 5 countries of interest.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>g5_ab</span> <span class='o'>&lt;-</span> <span class='k'>wca</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span>(<span class='k'>admin0Pcod</span> <span class='o'>%in%</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span>(<span class='s'>"BF"</span>, <span class='s'>"ML"</span>, <span class='s'>"NE"</span>, <span class='s'>"MR"</span>, <span class='s'>"TD"</span>))</code></pre>

</div>

We can now check our data by plotting it

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>g5_ab</span> <span class='o'>%&gt;%</span>
  <span class='nf'>ggplot</span>() <span class='o'>+</span>
  <span class='nf'>geom_sf</span>() <span class='o'>+</span>
  <span class='nf'>theme_minimal</span>()
</code></pre>
<img src="figs/unnamed-chunk-6-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Now that we have our background map, the next step is to get the *conflict data*. One of the main source for conflict data in the Sahel is [ACLED](https://www.acleddata.com/) and it can also be accessed from *HDX*. We will use the [`rhdx::search_datasets`](https://rdrr.io/pkg/rhdx/man/search_datasets.html) function since it allows to access multiple datasets as `list`. *HDX* is based on CKAN whose search is powered by [`SOLR`](http://lucene.apache.org/solr/). SOLR can be used to build complex queries to get exactly the datasets we want. In our case, we want conflict data from the [ACLED organization](http://data.humdata.org/organization/acled) in *HDX* and from the 5 countries (`group` in CKAN API)

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>solr_query</span> <span class='o'>&lt;-</span> <span class='s'>"organization:acled AND groups:(mli OR bfa OR tcd OR mrt OR ner)"</span>
<span class='k'>g5_acled</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/search_datasets.html'>search_datasets</a></span>(query = <span class='s'>"conflict data"</span>,
                            fq = <span class='k'>solr_query</span>)
<span class='k'>g5_acled</span>
<span class='c'>#&gt; [[1]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 331be608-def1-45c5-a9b0-36d4b3b4197e </span>
<span class='c'>#&gt;   Title: Chad - Conflict Data</span>
<span class='c'>#&gt;   Name: acled-data-for-chad</span>
<span class='c'>#&gt;   Date: 01/01/1997-12/31/2020</span>
<span class='c'>#&gt;   Tags (up to 5): hxl, protests, security incidents, violence and conflict</span>
<span class='c'>#&gt;   Locations (up to 5): tcd</span>
<span class='c'>#&gt;   Resources (up to 5): Conflict Data for Chad, QuickCharts-Conflict Data for Chad</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [[2]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 653dc159-097d-4fef-9527-53ee30d132ff </span>
<span class='c'>#&gt;   Title: Niger - Conflict Data</span>
<span class='c'>#&gt;   Name: acled-data-for-niger</span>
<span class='c'>#&gt;   Date: 01/01/1997-12/31/2020</span>
<span class='c'>#&gt;   Tags (up to 5): hxl, protests, security incidents, violence and conflict</span>
<span class='c'>#&gt;   Locations (up to 5): ner</span>
<span class='c'>#&gt;   Resources (up to 5): Conflict Data for Niger, QuickCharts-Conflict Data for Niger</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [[3]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 537ee9a6-ff76-4e00-a9f6-1c7c16ae1628 </span>
<span class='c'>#&gt;   Title: Mauritania - Conflict Data</span>
<span class='c'>#&gt;   Name: acled-data-for-mauritania</span>
<span class='c'>#&gt;   Date: 01/01/1997-12/31/2020</span>
<span class='c'>#&gt;   Tags (up to 5): hxl, protests, security incidents, violence and conflict</span>
<span class='c'>#&gt;   Locations (up to 5): mrt</span>
<span class='c'>#&gt;   Resources (up to 5): Conflict Data for Mauritania, QuickCharts-Conflict Data for Mauritania</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [[4]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 5895de63-010c-4716-97cb-fbdd3caf4e3a </span>
<span class='c'>#&gt;   Title: Mali - Conflict Data</span>
<span class='c'>#&gt;   Name: acled-data-for-mali</span>
<span class='c'>#&gt;   Date: 01/01/1997-12/31/2020</span>
<span class='c'>#&gt;   Tags (up to 5): hxl, protests, security incidents, violence and conflict</span>
<span class='c'>#&gt;   Locations (up to 5): mli</span>
<span class='c'>#&gt;   Resources (up to 5): Conflict Data for Mali, QuickCharts-Conflict Data for Mali</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [[5]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 6913ddaf-1ad2-4cad-b178-592b6d49cd61 </span>
<span class='c'>#&gt;   Title: Burkina Faso - Conflict Data</span>
<span class='c'>#&gt;   Name: acled-data-for-burkina-faso</span>
<span class='c'>#&gt;   Date: 01/01/1997-12/31/2020</span>
<span class='c'>#&gt;   Tags (up to 5): hxl, protests, security incidents, violence and conflict</span>
<span class='c'>#&gt;   Locations (up to 5): bfa</span>
<span class='c'>#&gt;   Resources (up to 5): Conflict Data for Burkina Faso, QuickCharts-Conflict Data for Burkina Faso</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [[6]]</span>
<span class='c'>#&gt; &lt;HDX Dataset&gt; 71d852e4-e41e-4320-a770-9fc2bb87fb64 </span>
<span class='c'>#&gt;   Title: ACLED Conflict Data for Africa 1997-2016</span>
<span class='c'>#&gt;   Name: acled-conflict-data-for-africa-1997-lastyear</span>
<span class='c'>#&gt;   Date: 01/01/2017</span>
<span class='c'>#&gt;   Tags (up to 5): geodata, protests, violence and conflict, vulnerable populations</span>
<span class='c'>#&gt;   Locations (up to 5): dza, ago, ben, bwa, bfa</span>
<span class='c'>#&gt;   Resources (up to 5): ACLED-Version-7-All-Africa-1997-2016_csv_dyadic-file.zip, ACLED-Version-7-All-Africa-1997-2016_dyadic-file.xlsx, ACLED-Version-7-All-Africa-1997-2016_monadic-file_csv.zip, ACLED-Version-7-All-Africa-1997-2016_monadic-file-1.xlsx, ACLED-Version-7-All-Africa-1997-2016_actordyad_csv.zip</span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; attr(,"class")</span>
<span class='c'>#&gt; [1] "datasets_list"</span></code></pre>

</div>

We will select the first 5 datasets (our 5 countries) in the list of datasets and bind them together using [`purrr::map_df`](https://purrr.tidyverse.org/reference/map.html) and a helper function.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>g5_acled</span> <span class='o'>&lt;-</span> <span class='k'>g5_acled</span>[<span class='m'>1</span><span class='o'>:</span><span class='m'>5</span>] <span class='c'>## pick the first 5 the 6th is the Africa wide dataset</span>

<span class='c'>## create a helper function to read resources from each dataset</span>
<span class='k'>read_acled_data</span> <span class='o'>&lt;-</span> <span class='nf'>function</span>(<span class='k'>dataset</span>) {
  <span class='k'>dataset</span> <span class='o'>%&gt;%</span>
    <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/get_resource.html'>get_resource</a></span>(<span class='m'>1</span>) <span class='o'>%&gt;%</span>
    <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/read_resource.html'>read_resource</a></span>(force_download = <span class='kc'>TRUE</span>)
}

<span class='k'>g5_acled_data</span> <span class='o'>&lt;-</span> <span class='nf'>map_df</span>(<span class='k'>g5_acled</span>, <span class='k'>read_acled_data</span>)
<span class='nf'>glimpse</span>(<span class='k'>g5_acled_data</span>)
<span class='c'>#&gt; Rows: 9,534</span>
<span class='c'>#&gt; Columns: 31</span>
<span class='c'>#&gt; $ data_id          <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 7129882, 7129881, 7126764, 7130217, 7…</span></span>
<span class='c'>#&gt; $ iso              <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 148, 148, 148, 148, 148, 148, 148, 14…</span></span>
<span class='c'>#&gt; $ event_id_cnty    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "CHA1096", "CHA1095", "CHA1093", "CHA…</span></span>
<span class='c'>#&gt; $ event_id_no_cnty <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 1096, 1095, 1093, 1094, 1092, 1091, 1…</span></span>
<span class='c'>#&gt; $ event_date       <span style='color: #555555;font-style: italic;'>&lt;date&gt;</span><span> 2020-06-19, 2020-06-17, 2020-06-13, …</span></span>
<span class='c'>#&gt; $ year             <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 2020, 2020, 2020, 2020, 2020, 2020, 2…</span></span>
<span class='c'>#&gt; $ time_precision   <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 1, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2…</span></span>
<span class='c'>#&gt; $ event_type       <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Battles", "Battles", "Riots", "Battl…</span></span>
<span class='c'>#&gt; $ sub_event_type   <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Armed clash", "Armed clash", "Mob vi…</span></span>
<span class='c'>#&gt; $ actor1           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Unidentified Armed Group (Chad)", "U…</span></span>
<span class='c'>#&gt; $ assoc_actor_1    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, "Vigilante Group (Chad)", NA,…</span></span>
<span class='c'>#&gt; $ inter1           <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 3, 3, 5, 2, 4, 1, 4, 4, 2, 2, 2, 1, 4…</span></span>
<span class='c'>#&gt; $ actor2           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Militia (Miners)", "Militia (Miners)…</span></span>
<span class='c'>#&gt; $ assoc_actor_2    <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> NA, NA, NA, NA, "Farmers (Chad)", NA,…</span></span>
<span class='c'>#&gt; $ inter2           <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 3, 3, 7, 4, 7, 8, 7, 4, 1, 1, 1, 7, 4…</span></span>
<span class='c'>#&gt; $ interaction      <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 33, 33, 57, 24, 47, 18, 47, 44, 12, 1…</span></span>
<span class='c'>#&gt; $ region           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Middle Africa", "Middle Africa", "Mi…</span></span>
<span class='c'>#&gt; $ country          <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Chad", "Chad", "Chad", "Chad", "Chad…</span></span>
<span class='c'>#&gt; $ admin1           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Tibesti", "Tibesti", "N'Djamena", "L…</span></span>
<span class='c'>#&gt; $ admin2           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Tibesti Est", "Tibesti Est", "N'Djam…</span></span>
<span class='c'>#&gt; $ admin3           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Tibesti", "Tibesti", "Ndjamena", "Ng…</span></span>
<span class='c'>#&gt; $ location         <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Zouarke", "Bardai", "NDjamena", "Bar…</span></span>
<span class='c'>#&gt; $ latitude         <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 20.4174, 21.3580, 12.1085, 13.5127, 9…</span></span>
<span class='c'>#&gt; $ longitude        <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 16.2238, 17.0010, 15.0482, 13.8501, 1…</span></span>
<span class='c'>#&gt; $ geo_precision    <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 2, 3, 1, 1, 2, 1, 2, 2, 2, 1, 1, 2, 1…</span></span>
<span class='c'>#&gt; $ source           <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "Alwihda (Chad)", "Alwihda (Chad)", "…</span></span>
<span class='c'>#&gt; $ source_scale     <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "National", "National", "National", "…</span></span>
<span class='c'>#&gt; $ notes            <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "On 19 June 2020, gold miners and sus…</span></span>
<span class='c'>#&gt; $ fatalities       <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 14, 10, 1, 0, 1, 0, 0, 2, 7, 2, 2, 1,…</span></span>
<span class='c'>#&gt; $ timestamp        <span style='color: #555555;font-style: italic;'>&lt;dbl&gt;</span><span> 1592852504, 1592852504, 1592254856, 1…</span></span>
<span class='c'>#&gt; $ iso3             <span style='color: #555555;font-style: italic;'>&lt;chr&gt;</span><span> "TCD", "TCD", "TCD", "TCD", "TCD", "T…</span></span></code></pre>

</div>

We have all the data we need for our analysis, we just need to aggregate total number fatalities by geographical coordinates, countries and year.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>g5_acled_fatalities_loc</span> <span class='o'>&lt;-</span> <span class='k'>g5_acled_data</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span>(<span class='k'>year</span> <span class='o'>%in%</span> <span class='m'>2012</span><span class='o'>:</span><span class='m'>2018</span>, <span class='k'>fatalities</span> <span class='o'>&gt;</span> <span class='m'>0</span>) <span class='o'>%&gt;%</span>
  <span class='nf'>group_by</span>(<span class='k'>year</span>, <span class='k'>country</span>, <span class='k'>latitude</span>, <span class='k'>longitude</span>) <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/sf/man/tidyverse.html'>summarise</a></span>(total_fatalities = <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span>(<span class='k'>fatalities</span>, na.rm = <span class='kc'>TRUE</span>)) <span class='o'>%&gt;%</span>
  <span class='nf'>arrange</span>(<span class='k'>year</span>) <span class='o'>%&gt;%</span>
  <span class='nf'>ungroup</span>()</code></pre>

</div>

We can finally use our boundaries (`g5_ab`), the conflict data (`g5_acled_fatalities_loc`) with `gganimate` to dynamically visualize the different fatal incidents in G5 Sahel countries.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>g5_ab</span> <span class='o'>%&gt;%</span>
  <span class='nf'>ggplot</span>() <span class='o'>+</span>
  <span class='nf'>geom_sf</span>(fill = <span class='s'>"#383838"</span>, color = <span class='s'>"gray"</span>) <span class='o'>+</span>
  <span class='nf'>coord_sf</span>(datum = <span class='m'>NA</span>) <span class='o'>+</span>
  <span class='nf'>geom_point</span>(data = <span class='k'>g5_acled_fatalities_loc</span>,
             <span class='nf'>aes</span>(x = <span class='k'>longitude</span>,
                 y = <span class='k'>latitude</span>,
                 size = <span class='k'>total_fatalities</span>,
                 fill = <span class='k'>total_fatalities</span>),
             shape = <span class='m'>21</span>,
             color = <span class='s'>"transparent"</span>) <span class='o'>+</span>
  <span class='nf'>scale_fill_viridis_c</span>(option = <span class='s'>"plasma"</span>) <span class='o'>+</span>
  <span class='nf'>geom_sf_text</span>(<span class='nf'>aes</span>(label = <span class='k'>admin0Name</span>), color = <span class='s'>"gray"</span>, fontface = <span class='s'>"bold"</span>) <span class='o'>+</span>
  <span class='nf'>labs</span>(x = <span class='s'>""</span>,
       y = <span class='s'>""</span>,
       title = <span class='s'>"Fatal events in the Sahel G5"</span>,
       subtitle = <span class='s'>"for the year {current_frame}"</span>,
       caption = <span class='s'>"source: ACLED"</span>) <span class='o'>+</span>
  <span class='nf'>theme_void</span>() <span class='o'>+</span>
  <span class='nf'>theme</span>(legend.position = <span class='s'>"none"</span>) <span class='o'>+</span>
  <span class='nf'><a href='https://gganimate.com/reference/transition_manual.html'>transition_manual</a></span>(<span class='k'>year</span>, cumulative = <span class='kc'>TRUE</span>) <span class='o'>+</span>
  <span class='nf'><a href='https://gganimate.com/reference/shadow_mark.html'>shadow_mark</a></span>()</code></pre>

</div>

<div class="highlight">

<img src="https://gitlab.com/dickoa/dickoa.gitlab.io/raw/master/static/img/sahel-facilities_files/anim_map-1.gif" width="100%" style="display: block; margin: auto;" />

</div>

Session info for this analysis.

<details>

<summary>Session info</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='k'>devtools</span>::<span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/session_info.html'>session_info</a></span>()
<span class='c'>#&gt; ─ Session info ────────────────────────────────────────────────</span>
<span class='c'>#&gt;  setting  value                                      </span>
<span class='c'>#&gt;  version  R version 4.0.2 Patched (2020-06-22 r78732)</span>
<span class='c'>#&gt;  os       Arch Linux                                 </span>
<span class='c'>#&gt;  system   x86_64, linux-gnu                          </span>
<span class='c'>#&gt;  ui       X11                                        </span>
<span class='c'>#&gt;  language (EN)                                       </span>
<span class='c'>#&gt;  collate  en_US.UTF-8                                </span>
<span class='c'>#&gt;  ctype    en_US.UTF-8                                </span>
<span class='c'>#&gt;  tz       Africa/Abidjan                             </span>
<span class='c'>#&gt;  date     2020-06-26                                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; ─ Packages ────────────────────────────────────────────────────</span>
<span class='c'>#&gt;  package     * version    date       lib</span>
<span class='c'>#&gt;  assertthat    0.2.1      2019-03-21 [1]</span>
<span class='c'>#&gt;  backports     1.1.8      2020-06-17 [1]</span>
<span class='c'>#&gt;  base64enc     0.1-3      2015-07-28 [1]</span>
<span class='c'>#&gt;  blob          1.2.1      2020-01-20 [1]</span>
<span class='c'>#&gt;  broom         0.5.6      2020-04-20 [1]</span>
<span class='c'>#&gt;  callr         3.4.3      2020-03-28 [1]</span>
<span class='c'>#&gt;  cellranger    1.1.0      2016-07-27 [1]</span>
<span class='c'>#&gt;  class         7.3-17     2020-04-26 [1]</span>
<span class='c'>#&gt;  classInt      0.4-3      2020-04-07 [1]</span>
<span class='c'>#&gt;  cli           2.0.2      2020-02-28 [1]</span>
<span class='c'>#&gt;  colorspace    1.4-1      2019-03-18 [1]</span>
<span class='c'>#&gt;  crayon        1.3.4      2017-09-16 [1]</span>
<span class='c'>#&gt;  crul          0.9.2.92   2020-06-17 [1]</span>
<span class='c'>#&gt;  curl          4.3        2019-12-02 [1]</span>
<span class='c'>#&gt;  DBI           1.1.0      2019-12-15 [1]</span>
<span class='c'>#&gt;  dbplyr        1.4.4      2020-05-27 [1]</span>
<span class='c'>#&gt;  desc          1.2.0      2018-05-01 [1]</span>
<span class='c'>#&gt;  devtools      2.3.0      2020-04-10 [1]</span>
<span class='c'>#&gt;  digest        0.6.25     2020-02-23 [1]</span>
<span class='c'>#&gt;  downlit       0.0.0.9000 2020-06-24 [1]</span>
<span class='c'>#&gt;  dplyr       * 1.0.0      2020-05-29 [1]</span>
<span class='c'>#&gt;  e1071         1.7-3      2019-11-26 [1]</span>
<span class='c'>#&gt;  ellipsis      0.3.1      2020-05-15 [1]</span>
<span class='c'>#&gt;  evaluate      0.14       2019-05-28 [1]</span>
<span class='c'>#&gt;  fansi         0.4.1      2020-01-08 [1]</span>
<span class='c'>#&gt;  farver        2.0.3      2020-01-16 [1]</span>
<span class='c'>#&gt;  forcats     * 0.5.0      2020-03-01 [1]</span>
<span class='c'>#&gt;  fs            1.4.1      2020-04-04 [1]</span>
<span class='c'>#&gt;  generics      0.0.2      2018-11-29 [1]</span>
<span class='c'>#&gt;  gganimate   * 1.0.5      2020-02-09 [1]</span>
<span class='c'>#&gt;  ggplot2     * 3.3.2      2020-06-19 [1]</span>
<span class='c'>#&gt;  gifski        0.8.6      2018-09-28 [1]</span>
<span class='c'>#&gt;  glue          1.4.1      2020-05-13 [1]</span>
<span class='c'>#&gt;  gtable        0.3.0      2019-03-25 [1]</span>
<span class='c'>#&gt;  haven         2.3.1      2020-06-01 [1]</span>
<span class='c'>#&gt;  hms           0.5.3      2020-01-08 [1]</span>
<span class='c'>#&gt;  hoardr        0.5.2.93   2020-06-08 [1]</span>
<span class='c'>#&gt;  htmltools     0.5.0      2020-06-16 [1]</span>
<span class='c'>#&gt;  httpcode      0.3.0      2020-04-10 [1]</span>
<span class='c'>#&gt;  httr          1.4.1      2019-08-05 [1]</span>
<span class='c'>#&gt;  hugodown      0.0.0.9000 2020-06-24 [1]</span>
<span class='c'>#&gt;  jsonlite      1.7.0      2020-06-25 [1]</span>
<span class='c'>#&gt;  KernSmooth    2.23-17    2020-04-26 [1]</span>
<span class='c'>#&gt;  knitr         1.29       2020-06-23 [1]</span>
<span class='c'>#&gt;  lattice       0.20-41    2020-04-02 [1]</span>
<span class='c'>#&gt;  lifecycle     0.2.0      2020-03-06 [1]</span>
<span class='c'>#&gt;  lubridate     1.7.9      2020-06-08 [1]</span>
<span class='c'>#&gt;  magrittr      1.5        2014-11-22 [1]</span>
<span class='c'>#&gt;  memoise       1.1.0      2017-04-21 [1]</span>
<span class='c'>#&gt;  modelr        0.1.8      2020-05-19 [1]</span>
<span class='c'>#&gt;  munsell       0.5.0      2018-06-12 [1]</span>
<span class='c'>#&gt;  nlme          3.1-148    2020-05-24 [1]</span>
<span class='c'>#&gt;  pillar        1.4.4      2020-05-05 [1]</span>
<span class='c'>#&gt;  pkgbuild      1.0.8      2020-05-07 [1]</span>
<span class='c'>#&gt;  pkgconfig     2.0.3      2019-09-22 [1]</span>
<span class='c'>#&gt;  pkgload       1.1.0      2020-05-29 [1]</span>
<span class='c'>#&gt;  prettyunits   1.1.1      2020-01-24 [1]</span>
<span class='c'>#&gt;  processx      3.4.2      2020-02-09 [1]</span>
<span class='c'>#&gt;  progress      1.2.2      2019-05-16 [1]</span>
<span class='c'>#&gt;  ps            1.3.3      2020-05-08 [1]</span>
<span class='c'>#&gt;  purrr       * 0.3.4      2020-04-17 [1]</span>
<span class='c'>#&gt;  R6            2.4.1      2019-11-12 [1]</span>
<span class='c'>#&gt;  rappdirs      0.3.1      2016-03-28 [1]</span>
<span class='c'>#&gt;  Rcpp          1.0.4.12   2020-06-16 [1]</span>
<span class='c'>#&gt;  readr       * 1.3.1      2018-12-21 [1]</span>
<span class='c'>#&gt;  readxl        1.3.1      2019-03-13 [1]</span>
<span class='c'>#&gt;  remotes       2.1.1      2020-02-15 [1]</span>
<span class='c'>#&gt;  reprex        0.3.0      2019-05-16 [1]</span>
<span class='c'>#&gt;  rhdx        * 0.1.0.9000 2020-06-14 [1]</span>
<span class='c'>#&gt;  rlang         0.4.6      2020-05-02 [1]</span>
<span class='c'>#&gt;  rmarkdown     2.3.1      2020-06-24 [1]</span>
<span class='c'>#&gt;  rprojroot     1.3-2      2018-01-03 [1]</span>
<span class='c'>#&gt;  rstudioapi    0.11       2020-02-07 [1]</span>
<span class='c'>#&gt;  rvest         0.3.5      2019-11-08 [1]</span>
<span class='c'>#&gt;  scales        1.1.1      2020-05-11 [1]</span>
<span class='c'>#&gt;  sessioninfo   1.1.1      2018-11-05 [1]</span>
<span class='c'>#&gt;  sf          * 0.9-4      2020-06-19 [1]</span>
<span class='c'>#&gt;  stringi       1.4.6      2020-02-17 [1]</span>
<span class='c'>#&gt;  stringr     * 1.4.0      2019-02-10 [1]</span>
<span class='c'>#&gt;  testthat      2.3.2      2020-03-02 [1]</span>
<span class='c'>#&gt;  tibble      * 3.0.1      2020-04-20 [1]</span>
<span class='c'>#&gt;  tidyr       * 1.1.0      2020-05-20 [1]</span>
<span class='c'>#&gt;  tidyselect    1.1.0      2020-05-11 [1]</span>
<span class='c'>#&gt;  tidyverse   * 1.3.0      2019-11-21 [1]</span>
<span class='c'>#&gt;  triebeard     0.3.0      2016-08-04 [1]</span>
<span class='c'>#&gt;  tweenr        1.0.1      2018-12-14 [1]</span>
<span class='c'>#&gt;  units         0.6-7      2020-06-13 [1]</span>
<span class='c'>#&gt;  urltools      1.7.3      2019-04-14 [1]</span>
<span class='c'>#&gt;  usethis       1.6.1      2020-04-29 [1]</span>
<span class='c'>#&gt;  utf8          1.1.4      2018-05-24 [1]</span>
<span class='c'>#&gt;  vctrs         0.3.1      2020-06-05 [1]</span>
<span class='c'>#&gt;  withr         2.2.0      2020-04-20 [1]</span>
<span class='c'>#&gt;  xfun          0.15       2020-06-21 [1]</span>
<span class='c'>#&gt;  xml2          1.3.2      2020-04-23 [1]</span>
<span class='c'>#&gt;  yaml          2.2.1      2020-02-01 [1]</span>
<span class='c'>#&gt;  source                            </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-lib/downlit@9191e1f)    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-lib/hugodown@f7df565)   </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  local                             </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (rstudio/rmarkdown@b53a85a)</span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  Github (r-spatial/sf@0b08ed5)     </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.1)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt;  CRAN (R 4.0.0)                    </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /usr/lib/R/library</span></code></pre>

</div>

</details>

