---
output: hugodown::md_document

title: "Flood mapping and rapid impact assessment in Niamey"
author: "Ahmadou Dicko"
date: "2020-09-20"

tags:
  - gis, sf, stars
rmd_hash: 9c5157806800dfa7

---

Climate change already impacts the Sahel region with an increase in the occurrence of extreme events such as floods. In Niger, these floods events usually happens along the <a href="https://en.wikipedia.org/wiki/Niger_River" target="_blank">Niger river</a>. In Niamey, the heavy rains of the end of August and early September resulted in extreme flooding along the river path.

<div class="highlight">

<div class="figure" style="text-align: center">

<img src="./figs/niger-floods.jpg" alt="homes in Niger inundated by heavy flooding.Source: Getty Images" width="700px" />
<p class="caption">
homes in Niger inundated by heavy flooding.Source: Getty Images
</p>

</div>

</div>

In this post, we will develop a reproducible workflow for a rapid impact assessment of flooding using <a href="https://en.wikipedia.org/wiki/Sentinel-2" target="_blank">Sentintel S2</a> data.

R packages required for the analysis
------------------------------------

The following packages will be used for this analysis.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://tidyverse.tidyverse.org'>tidyverse</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://r-spatial.github.io/sf/'>sf</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://r-spatial.github.io/stars/'>stars</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://sen2r.ranghetti.info'>sen2r</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://gitlab.com/dickoa/rhdx'>rhdx</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/r-spatial/mapview'>mapview</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/r-quantities/units/'>units</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='https://github.com/anthonynorth/snapbox'>snapbox</a></span><span class='o'>)</span>
<span class='kr'><a href='https://rdrr.io/r/base/library.html'>library</a></span><span class='o'>(</span><span class='nv'><a href='http://github.com/jrnold/ggthemes'>ggthemes</a></span><span class='o'>)</span>
</code></pre>

</div>

The packages `rhdx` and `snapbox` are not yet on CRAN, you will need the `remotes` package to get them.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>remotes</span><span class='nf'>::</span><span class='nf'><a href='https://remotes.r-lib.org/reference/install_github.html'>install_github</a></span><span class='o'>(</span><span class='s'>"dickoa/rhdx"</span><span class='o'>)</span>
<span class='nf'>remotes</span><span class='nf'>::</span><span class='nf'><a href='https://remotes.r-lib.org/reference/install_github.html'>install_github</a></span><span class='o'>(</span><span class='s'>"anthonynorth/snapbox"</span><span class='o'>)</span>
</code></pre>

</div>

This analysis is based on a <a href="http://www.un-spider.org/advisory-support/recommended-practices/flood-mapping-and-damage-assessment-using-s2-data/step-by-step" target="_blank">UN-SPIDER tutorial</a> on flood mapping using Sentintel S2 data. In the original article, `QGIS` was used to map flood extent, here, we are going to do the same analysis using `R`.

Area of interest
----------------

We will restrict this analysis to Niamey and its surrounding where the flood impacted many people lives. The bounding box of the study area can be [downloaded here](https://gitlab.com/uploads/-/system/personal_snippet/2027073/c80d9d7bc55ba91f1a485aa84c2c25be/bbox.fgb).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>bbox_url</span> <span class='o'>&lt;-</span> <span class='s'>"https://tinyurl.com/yyobbc58"</span>
<span class='nv'>bbox</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_read.html'>read_sf</a></span><span class='o'>(</span><span class='nv'>bbox_url</span><span class='o'>)</span>
<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://rdrr.io/pkg/snapbox/man/layer_mapbox.html'>layer_mapbox</a></span><span class='o'>(</span><span class='nv'>bbox</span>,
               map_style <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/stylebox/man/mapbox_styles.html'>mapbox_satellite_streets</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>

</code></pre>
<img src="figs/bbox-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Permanent Water in Niamey
-------------------------

In order to map flood extent, we need to compare the waterbody mapped to a baseline of permanent water. <a href="https://global-surface-water.appspot.com/" target="_blank">JRC Global Surface Water</a> is an excellent source to access surface water. Permanent water surfaces are defined using the water occurence layer which is the frequency with which water was present in a given pixel from March 1984 to December 2019. Water surfaces are permanent in pixel when it reaches the frequency 100. It means that for this pixel contained water surfaces over the 36 years timespan. In this analysis, the definition is relaxed and permanent water surfaces is defined by water with 80% occurence over 36 years. The data is available <a href="https://global-surface-water.appspot.com/download" target="_blank">online</a> where it can be downloaded.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>occurence</span> <span class='o'>&lt;-</span> <span class='s'>"https://storage.googleapis.com/global-surface-water/downloads2019v2/occurrence/occurrence_0E_20Nv1_1_2019.tif"</span>
<span class='nv'>occ</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/stars/reference/read_stars.html'>read_stars</a></span><span class='o'>(</span><span class='nv'>occurence</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crop.html'>st_crop</a></span><span class='o'>(</span><span class='nv'>bbox</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/stars/reference/st_as_stars.html'>st_as_stars</a></span><span class='o'>(</span><span class='o'>)</span>
<span class='nv'>occ</span>

<span class='c'>#&gt; stars object with 2 dimensions and 1 attribute</span>
<span class='c'>#&gt; attribute(s), summary of first 1e+05 cells:</span>
<span class='c'>#&gt;  occurrence_0E_20Nv1_1_2019.tif </span>
<span class='c'>#&gt;  1      :95865                  </span>
<span class='c'>#&gt;  91     :  324                  </span>
<span class='c'>#&gt;  90     :  299                  </span>
<span class='c'>#&gt;  89     :  268                  </span>
<span class='c'>#&gt;  88     :  151                  </span>
<span class='c'>#&gt;  (Other): 2964                  </span>
<span class='c'>#&gt;  NA's   :  129                  </span>
<span class='c'>#&gt; dimension(s):</span>
<span class='c'>#&gt;    from    to offset    delta refsys point values x/y</span>
<span class='c'>#&gt; x  7889  9434      0  0.00025 WGS 84 FALSE   NULL [x]</span>
<span class='c'>#&gt; y 25624 26866     20 -0.00025 WGS 84 FALSE   NULL [y]</span>
</code></pre>

</div>

Let's transform the `occ` raster layer to build build our permanent water layer.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>mat</span> <span class='o'>&lt;-</span> <span class='nv'>occ</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>
<span class='nf'><a href='https://rdrr.io/r/utils/str.html'>str</a></span><span class='o'>(</span><span class='nv'>mat</span><span class='o'>)</span>

<span class='c'>#&gt;  Factor[1:1546, 1:1243] w/ 256 levels "1","2","3","4",..: NA 1 1 1 1 1 1 1 1 1 ...</span>
<span class='c'>#&gt;  - attr(*, "colors")= chr [1:256] "#FFFFFFFF" "#FEFCFCFF" "#FEF9FAFF" "#FEF7F7FF" ...</span>
</code></pre>

</div>

The data is of <a href="https://stat.ethz.ch/R-manual/R-devel/library/base/html/factor.html" target="_blank"><code>factor</code></a> type, to easily subset the data, we need to convert the type to `numeric`. Particularly, selecting pixels with an occurence of 80 will be achieved more efficiently with `numeric` vectors.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>mat</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/numeric.html'>as.numeric</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/character.html'>as.character</a></span><span class='o'>(</span><span class='nv'>mat</span><span class='o'>)</span><span class='o'>)</span>
<span class='nv'>mat</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/matrix.html'>matrix</a></span><span class='o'>(</span><span class='nv'>mat</span>, nrow <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/dim.html'>dim</a></span><span class='o'>(</span><span class='nv'>occ</span><span class='o'>)</span><span class='o'>[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>)</span>
<span class='nv'>mat</span><span class='o'>[</span><span class='nv'>mat</span> <span class='o'>&lt;</span> <span class='m'>80</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='kc'>NA</span>
<span class='nv'>mat</span><span class='o'>[</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/NA.html'>is.na</a></span><span class='o'>(</span><span class='nv'>mat</span><span class='o'>)</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='m'>1L</span>
<span class='nv'>occ</span><span class='o'>$</span><span class='nv'>water_perm</span> <span class='o'>&lt;-</span> <span class='nv'>mat</span>
<span class='nv'>occ</span>

<span class='c'>#&gt; stars object with 2 dimensions and 2 attributes</span>
<span class='c'>#&gt; attribute(s), summary of first 1e+05 cells:</span>
<span class='c'>#&gt;  occurrence_0E_20Nv1_1_2019.tif   water_perm    </span>
<span class='c'>#&gt;  1      :95865                   Min.   :1      </span>
<span class='c'>#&gt;  91     :  324                   1st Qu.:1      </span>
<span class='c'>#&gt;  90     :  299                   Median :1      </span>
<span class='c'>#&gt;  89     :  268                   Mean   :1      </span>
<span class='c'>#&gt;  88     :  151                   3rd Qu.:1      </span>
<span class='c'>#&gt;  (Other): 2964                   Max.   :1      </span>
<span class='c'>#&gt;  NA's   :  129                   NA's   :98221  </span>
<span class='c'>#&gt; dimension(s):</span>
<span class='c'>#&gt;    from    to offset    delta refsys point values x/y</span>
<span class='c'>#&gt; x  7889  9434      0  0.00025 WGS 84 FALSE   NULL [x]</span>
<span class='c'>#&gt; y 25624 26866     20 -0.00025 WGS 84 FALSE   NULL [y]</span>
</code></pre>

</div>

As a final step to have a permanent water layer, the raster layer `sais` need to turned into a spatial polygon for further analysis. In order to do this transformation, the `st_as_sf` function can be used to turn it the `occ` raster to a `sf` object.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>permanent_water</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_as_sf.html'>st_as_sf</a></span><span class='o'>(</span><span class='nv'>occ</span><span class='o'>[</span><span class='s'>"water_perm"</span><span class='o'>]</span>, merge <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>water_perm</span> <span class='o'>==</span> <span class='m'>1</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_combine.html'>st_union</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://rdrr.io/pkg/snapbox/man/layer_mapbox.html'>layer_mapbox</a></span><span class='o'>(</span><span class='nv'>bbox</span>,
               map_style <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/stylebox/man/mapbox_styles.html'>mapbox_satellite_streets</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>  <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggsf.html'>geom_sf</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>permanent_water</span>, fill <span class='o'>=</span> <span class='s'>"blue"</span>, size <span class='o'>=</span> <span class='m'>0.2</span><span class='o'>)</span>

</code></pre>
<img src="figs/permanent_water3-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Flood extent using Sentinel S2
------------------------------

A quick survey of the literature suggests that using the Normalized Difference Water Index (NDWI) index allow to easily spot waterbody content. In this analysis, ready to use bottom of atmosphere reflectances products (`BOA`) will also be used to make it simpler.

It's defined by the following formulas:

$$NDWI = \dfrac{X_{green} - X_{nir}}{X_{green} + X_{nir}}$$

Where $X_{green}$ is the green band (B3 at at 842 nm wavelength) and $X_{nir}$ is the near-infrared (NIR) band (B8, at 842 nm wavelength).

We will use the amazing <a href="http://sen2r.ranghetti.info/index.html" target="_blank"><code>sen2r</code></a> R package to download, pre-process and automatically compute the NDWI. In order to download Sentinel S2 data, you will need to register to the <a href="https://scihub.copernicus.eu/" target="_blank">Copernicus Open Access Hub</a> and provide your credentials to the [`sen2r::write_scihub_login`](http://sen2r.ranghetti.info/reference/scihub_login.html) function. This analysis will focus on the situation as of September 15th.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='http://sen2r.ranghetti.info/reference/scihub_login.html'>write_scihub_login</a></span><span class='o'>(</span>username <span class='o'>=</span> <span class='s'>"xxxxx"</span>,
                   password <span class='o'>=</span> <span class='s'>"xxxxx"</span><span class='o'>)</span>


<span class='nv'>out_dir</span> <span class='o'>&lt;-</span> <span class='s'>"./data/raster/s2out"</span>
<span class='nv'>safe_dir</span> <span class='o'>&lt;-</span> <span class='s'>"./data/raster/s2safe"</span>

<span class='kr'>if</span> <span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.exists</a></span><span class='o'>(</span><span class='nv'>out_dir</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.create</a></span><span class='o'>(</span><span class='nv'>out_dir</span>, recursive <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>

<span class='kr'>if</span> <span class='o'>(</span><span class='o'>!</span><span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.exists</a></span><span class='o'>(</span><span class='nv'>safe_dir</span><span class='o'>)</span><span class='o'>)</span>
  <span class='nf'><a href='https://rdrr.io/r/base/files2.html'>dir.create</a></span><span class='o'>(</span><span class='nv'>safe_dir</span>, recursive <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>

<span class='c'>###</span>
<span class='nv'>out_path</span> <span class='o'>&lt;-</span> <span class='nf'><a href='http://sen2r.ranghetti.info/reference/sen2r.html'>sen2r</a></span><span class='o'>(</span>
  gui <span class='o'>=</span> <span class='kc'>FALSE</span>,
  step_atmcorr <span class='o'>=</span> <span class='s'>"l2a"</span>,
  extent <span class='o'>=</span> <span class='nv'>bbox</span>,
  extent_name <span class='o'>=</span> <span class='s'>"Niamey"</span>,
  timewindow <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='s'>"2020-09-10"</span><span class='o'>)</span>, <span class='nf'><a href='https://rdrr.io/r/base/as.Date.html'>as.Date</a></span><span class='o'>(</span><span class='s'>"2020-09-15"</span><span class='o'>)</span><span class='o'>)</span>,
  list_prods <span class='o'>=</span> <span class='s'>"BOA"</span>,
  list_indices <span class='o'>=</span> <span class='s'>"NDWI2"</span>,
  list_rgb <span class='o'>=</span> <span class='s'>"RGB432B"</span>,
  path_l1c <span class='o'>=</span> <span class='nv'>safe_dir</span>,
  path_l2a <span class='o'>=</span> <span class='nv'>safe_dir</span>,
  path_out <span class='o'>=</span> <span class='nv'>out_dir</span>,
  parallel <span class='o'>=</span> <span class='m'>8</span>,
  overwrite <span class='o'>=</span> <span class='kc'>TRUE</span>
<span class='o'>)</span>
</code></pre>

</div>

There's two versions of the NDWI index and the one we need is named `NDWI2` in the `sen2r` package.

<div class="highlight">

</div>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>ndwi</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/stars/reference/read_stars.html'>read_stars</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/file.path.html'>file.path</a></span><span class='o'>(</span><span class='s'>"data/raster/s2out"</span>,
                             <span class='s'>"NDWI2"</span>,
                             <span class='s'>"S2A2A_20200915_022_Niamey_NDWI2_10.tif"</span><span class='o'>)</span>,
                   proxy <span class='o'>=</span> <span class='kc'>FALSE</span><span class='o'>)</span>
<span class='nv'>ndwi</span>

<span class='c'>#&gt; stars object with 2 dimensions and 1 attribute</span>
<span class='c'>#&gt; attribute(s), summary of first 1e+05 cells:</span>
<span class='c'>#&gt;  S2A2A_20200915_022_Niamey_NDWI2_10.tif </span>
<span class='c'>#&gt;  Min.   :-7223                          </span>
<span class='c'>#&gt;  1st Qu.:-4323                          </span>
<span class='c'>#&gt;  Median :-3757                          </span>
<span class='c'>#&gt;  Mean   :-3674                          </span>
<span class='c'>#&gt;  3rd Qu.:-3067                          </span>
<span class='c'>#&gt;  Max.   : 1271                          </span>
<span class='c'>#&gt; dimension(s):</span>
<span class='c'>#&gt;   from   to  offset delta                refsys point values x/y</span>
<span class='c'>#&gt; x    1 4193  388660    10 WGS 84 / UTM zone 31N FALSE   NULL [x]</span>
<span class='c'>#&gt; y    1 3450 1503090   -10 WGS 84 / UTM zone 31N FALSE   NULL [y]</span>
</code></pre>

</div>

Looking at the `ndwi` data range, we can rescale the data by dividing by 10000 to simplify the rest of the analysis and map it to visualize this layer.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>ndwi</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span> <span class='o'>&lt;-</span> <span class='nv'>ndwi</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span><span class='o'>/</span><span class='m'>10000</span>

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://r-spatial.github.io/stars/reference/geom_stars.html'>geom_stars</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>ndwi</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_viridis.html'>scale_fill_viridis_c</a></span><span class='o'>(</span>guide <span class='o'>=</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/guide_legend.html'>guide_legend</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"NDWI"</span>,
                                            direction <span class='o'>=</span> <span class='s'>"horizontal"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/theme.html'>theme</a></span><span class='o'>(</span>legend.position <span class='o'>=</span> <span class='s'>"bottom"</span><span class='o'>)</span>

</code></pre>
<img src="figs/map_ndwi-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Binarize the NDWI
-----------------

The next step, is to turn this raster layer into a binary data. The theoretical threshold to select waterbody is set at 0 but we can be more precise and context-specific. We will thus sample NDWI values in pixels we know contains water surface using the `permanent_water` layer and average these values to get our data.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>permanent_water</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_transform.html'>st_transform</a></span><span class='o'>(</span><span class='nv'>permanent_water</span>, <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crs.html'>st_crs</a></span><span class='o'>(</span><span class='nv'>ndwi</span><span class='o'>)</span><span class='o'>)</span>
<span class='nv'>ndwi_water</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crop.html'>st_crop</a></span><span class='o'>(</span><span class='nv'>ndwi</span>,
                      <span class='nv'>permanent_water</span><span class='o'>)</span>
<span class='nv'>me</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/stats/median.html'>median</a></span><span class='o'>(</span><span class='nv'>ndwi_water</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
<span class='nv'>xbar</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/mean.html'>mean</a></span><span class='o'>(</span><span class='nv'>ndwi_water</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>, na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/c.html'>c</a></span><span class='o'>(</span>median <span class='o'>=</span> <span class='nv'>me</span>, average <span class='o'>=</span> <span class='nv'>xbar</span><span class='o'>)</span>

<span class='c'>#&gt;     median    average </span>
<span class='c'>#&gt; 0.05240000 0.05036571</span>
</code></pre>

</div>

All sampled pixels within the permanent water layer can be visualized using an histogram in order its distribution and variability.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://rdrr.io/r/graphics/hist.html'>hist</a></span><span class='o'>(</span><span class='nv'>ndwi_water</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span>, main <span class='o'>=</span> <span class='s'>""</span>, xlab <span class='o'>=</span> <span class='s'>"NDWI"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/graphics/abline.html'>abline</a></span><span class='o'>(</span>v <span class='o'>=</span> <span class='nv'>me</span>, col <span class='o'>=</span> <span class='s'>"red"</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/graphics/abline.html'>abline</a></span><span class='o'>(</span>v <span class='o'>=</span> <span class='nv'>xbar</span>, col <span class='o'>=</span> <span class='s'>"blue"</span><span class='o'>)</span>

</code></pre>
<img src="figs/perm_water_thresh-1.png" width="700px" style="display: block; margin: auto;" />

</div>

The distribution is unimodal and symmetric around the mode with low variability. Either the average or the median can be used as threshold. For the next step, the median will be used since it's more robust than the average in general.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>ndwi</span><span class='o'>$</span><span class='nv'>water_extent</span> <span class='o'>&lt;-</span> <span class='m'>1L</span> <span class='o'>*</span> <span class='o'>(</span><span class='nv'>ndwi</span><span class='o'>[[</span><span class='m'>1</span><span class='o'>]</span><span class='o'>]</span> <span class='o'>&gt;=</span> <span class='nv'>me</span><span class='o'>)</span>
<span class='nv'>ndwi</span>

<span class='c'>#&gt; stars object with 2 dimensions and 2 attributes</span>
<span class='c'>#&gt; attribute(s), summary of first 1e+05 cells:</span>
<span class='c'>#&gt;  S2A2A_20200915_022_Niamey_NDWI2_10.tif  water_extent   </span>
<span class='c'>#&gt;  Min.   :-0.7223                         Min.   :0e+00  </span>
<span class='c'>#&gt;  1st Qu.:-0.4323                         1st Qu.:0e+00  </span>
<span class='c'>#&gt;  Median :-0.3757                         Median :0e+00  </span>
<span class='c'>#&gt;  Mean   :-0.3674                         Mean   :7e-05  </span>
<span class='c'>#&gt;  3rd Qu.:-0.3067                         3rd Qu.:0e+00  </span>
<span class='c'>#&gt;  Max.   : 0.1271                         Max.   :1e+00  </span>
<span class='c'>#&gt; dimension(s):</span>
<span class='c'>#&gt;   from   to  offset delta                refsys point values x/y</span>
<span class='c'>#&gt; x    1 4193  388660    10 WGS 84 / UTM zone 31N FALSE   NULL [x]</span>
<span class='c'>#&gt; y    1 3450 1503090   -10 WGS 84 / UTM zone 31N FALSE   NULL [y]</span>
</code></pre>

</div>

For this data to be used it's need to be polygonized, we will follow some steps (comments in the code) to make sure the output is good enough for our analysis:

1.  Polygonize the raster
2.  Select the polygons containing water surfaces
3.  Remove polygons smaller than 500 square meters (avoid artificial water bodies, wet rooftops, etc)
4.  Dissolve and join all polygons into a single polygon

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>water_extent</span> <span class='o'>&lt;-</span> <span class='nv'>ndwi</span><span class='o'>[</span><span class='s'>"water_extent"</span><span class='o'>]</span> <span class='o'>%&gt;%</span>
  <span class='c'>## Polygonize the raster</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_as_sf.html'>st_as_sf</a></span><span class='o'>(</span>merge <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='c'>## select only water extent</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>water_extent</span> <span class='o'>==</span> <span class='m'>1</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='c'>## Remove polygons smaller than 500 sqm</span>
  <span class='nf'>mutate</span><span class='o'>(</span>area <span class='o'>=</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_measures.html'>st_area</a></span><span class='o'>(</span><span class='nv'>geometry</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span> <span class='c'>## compute areas</span>
  <span class='nf'><a href='https://rdrr.io/r/stats/filter.html'>filter</a></span><span class='o'>(</span><span class='nv'>area</span> <span class='o'>&gt;</span> <span class='nf'><a href='https://rdrr.io/pkg/units/man/set_units.html'>set_units</a></span><span class='o'>(</span><span class='m'>500</span>, <span class='nv'>m</span><span class='o'>^</span><span class='m'>2</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='c'>## Dissolve and join all polygons into a single polygon</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_unary.html'>st_buffer</a></span><span class='o'>(</span>dist <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/units/man/set_units.html'>set_units</a></span><span class='o'>(</span><span class='m'>0.5</span>, <span class='nv'>m</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_combine.html'>st_union</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_unary.html'>st_simplify</a></span><span class='o'>(</span>dTolerance <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/units/man/set_units.html'>set_units</a></span><span class='o'>(</span><span class='m'>10</span>, <span class='nv'>m</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_geometry.html'>st_set_geometry</a></span><span class='o'>(</span>value <span class='o'>=</span> <span class='nv'>.</span>, x <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/r/base/data.frame.html'>data.frame</a></span><span class='o'>(</span>id <span class='o'>=</span> <span class='m'>1</span><span class='o'>)</span><span class='o'>)</span>

<span class='nv'>water_extent</span>

<span class='c'>#&gt; Simple feature collection with 1 feature and 1 field</span>
<span class='c'>#&gt; geometry type:  MULTIPOLYGON</span>
<span class='c'>#&gt; dimension:      XY</span>
<span class='c'>#&gt; bbox:           xmin: 388659.5 ymin: 1468590 xmax: 430590.5 ymax: 1502860</span>
<span class='c'>#&gt; projected CRS:  WGS 84 / UTM zone 31N</span>
<span class='c'>#&gt;   id                       geometry</span>
<span class='c'>#&gt; 1  1 MULTIPOLYGON (((430470.5 14...</span>
</code></pre>

</div>

The `water_extent` derived from NDWI can now be analyzed and visualized.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://rdrr.io/pkg/snapbox/man/layer_mapbox.html'>layer_mapbox</a></span><span class='o'>(</span><span class='nv'>bbox</span>,
               map_style <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/stylebox/man/mapbox_styles.html'>mapbox_satellite_streets</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>  <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggsf.html'>geom_sf</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>water_extent</span>, fill <span class='o'>=</span> <span class='s'>"blue"</span>, size <span class='o'>=</span> <span class='m'>0.2</span><span class='o'>)</span>

</code></pre>
<img src="figs/plot_extent-1.png" width="700px" style="display: block; margin: auto;" />

</div>

The final step required to obtain the flood extent layer is to remove the permanent water extent (`permanent_water`) from the waterbody derived from NDWI (`water_extent`).

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>flood_extent</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_binary_ops.html'>st_difference</a></span><span class='o'>(</span><span class='nv'>water_extent</span>,
                              <span class='nv'>permanent_water</span><span class='o'>)</span>


<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://rdrr.io/pkg/snapbox/man/layer_mapbox.html'>layer_mapbox</a></span><span class='o'>(</span><span class='nv'>bbox</span>,
               map_style <span class='o'>=</span> <span class='nf'><a href='https://rdrr.io/pkg/stylebox/man/mapbox_styles.html'>mapbox_satellite_streets</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>  <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggsf.html'>geom_sf</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>flood_extent</span>, fill <span class='o'>=</span> <span class='s'>"blue"</span>, size <span class='o'>=</span> <span class='m'>0.2</span><span class='o'>)</span>

</code></pre>
<img src="figs/read_flood-1.png" width="700px" style="display: block; margin: auto;" />

</div>

Some statistics can be computed out of the `flood_extent` and one important metric is the total area of flooded zones within our area of interest.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>flood_extent</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_measures.html'>st_area</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/units/man/set_units.html'>set_units</a></span><span class='o'>(</span><span class='nv'>km</span><span class='o'>^</span><span class='m'>2</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>flood_area</span>
<span class='nv'>flood_area</span>

<span class='c'>#&gt; 44.84563 [km^2]</span>
</code></pre>

</div>

44.85 square kilometers of a total area of 1437 square kilometers is flooded as of September 15th.

A second figure of interest is the number of people potentially affected, to estimate this figure we need overlay our flood polygon (`flood_extent`) and some population count raster layer. In this analysis, we used the Facebook population density maps data for Niger. Facebook population density data are available on HDX and the `rhdx` can be used to access it.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>dir</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/r/base/basename.html'>dirname</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/tempfile.html'>tempdir</a></span><span class='o'>(</span><span class='o'>)</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/pull_dataset.html'>pull_dataset</a></span><span class='o'>(</span><span class='s'>"highresolutionpopulationdensitymaps-ner"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/get_resource.html'>get_resource</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/download_resource.html'>download_resource</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/utils/unzip.html'>unzip</a></span><span class='o'>(</span>exdir <span class='o'>=</span> <span class='nv'>dir</span><span class='o'>)</span>

<span class='nv'>ner_pop</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/stars/reference/read_stars.html'>read_stars</a></span><span class='o'>(</span><span class='nf'><a href='https://rdrr.io/r/base/file.path.html'>file.path</a></span><span class='o'>(</span><span class='nv'>dir</span>, <span class='s'>"population_ner_2018-10-01.tif"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crop.html'>st_crop</a></span><span class='o'>(</span><span class='nv'>bbox</span><span class='o'>)</span>

<span class='nf'><a href='https://rdrr.io/r/base/names.html'>names</a></span><span class='o'>(</span><span class='nv'>ner_pop</span><span class='o'>)</span> <span class='o'>&lt;-</span> <span class='s'>"ner_pop"</span>

<span class='nf'><a href='https://ggplot2.tidyverse.org/reference/ggplot.html'>ggplot</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://r-spatial.github.io/stars/reference/geom_stars.html'>geom_stars</a></span><span class='o'>(</span>data <span class='o'>=</span> <span class='nv'>ner_pop</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/scale_viridis.html'>scale_fill_viridis_c</a></span><span class='o'>(</span>guide <span class='o'>=</span> <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/guide_legend.html'>guide_legend</a></span><span class='o'>(</span>title <span class='o'>=</span> <span class='s'>"Population count 2018"</span>,
                                            direction <span class='o'>=</span> <span class='s'>"horizontal"</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>+</span>
  <span class='nf'><a href='https://ggplot2.tidyverse.org/reference/theme.html'>theme</a></span><span class='o'>(</span>legend.position <span class='o'>=</span> <span class='s'>"bottom"</span><span class='o'>)</span>

</code></pre>
<img src="figs/fb_grid-1.png" width="700px" style="display: block; margin: auto;" />

</div>

We now have the population grid, for each pixel of 30x30m it estimates the population count at that given pixel. To have the population affected, one can sum the values of all pixels falling into our flood extent layer.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>ner_pop</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/stars/reference/st_as_stars.html'>st_as_stars</a></span><span class='o'>(</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crop.html'>st_crop</a></span><span class='o'>(</span><span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_transform.html'>st_transform</a></span><span class='o'>(</span><span class='nv'>flood_extent</span>, <span class='m'>4326</span><span class='o'>)</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'>pull</span><span class='o'>(</span><span class='nv'>ner_pop</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/r/base/sum.html'>sum</a></span><span class='o'>(</span>na.rm <span class='o'>=</span> <span class='kc'>TRUE</span><span class='o'>)</span> <span class='o'>-&gt;</span> <span class='nv'>pop_affected</span>
<span class='nv'>pop_affected</span>

<span class='c'>#&gt; [1] 4512.976</span>
</code></pre>

</div>

We estimate around 4513 affected persons in Niamey and its surroundings. We can also estimate the number of affected buildings. A good source for buildings data is OpenStreetMap (OSM) and Niger has one vibrant OSM community, so the data is of good quality particularly in cities like Niamey. Once again, the data is available on HDX.

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nv'>ner_bu</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/pull_dataset.html'>pull_dataset</a></span><span class='o'>(</span><span class='s'>"hotosm_niger_buildings"</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/get_resource.html'>get_resource</a></span><span class='o'>(</span><span class='m'>1</span><span class='o'>)</span> <span class='o'>%&gt;%</span>
  <span class='nf'><a href='https://rdrr.io/pkg/rhdx/man/read_resource.html'>read_resource</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='nv'>ner_bu</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_transform.html'>st_transform</a></span><span class='o'>(</span><span class='nv'>ner_bu</span>, <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crs.html'>st_crs</a></span><span class='o'>(</span><span class='nv'>flood_extent</span><span class='o'>)</span><span class='o'>)</span>

<span class='nv'>ner_bu</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/st_crop.html'>st_crop</a></span><span class='o'>(</span><span class='nv'>ner_bu</span>, <span class='nv'>flood_extent</span><span class='o'>)</span>
<span class='nv'>ner_bu_centroid</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_unary.html'>st_point_on_surface</a></span><span class='o'>(</span><span class='nv'>ner_bu</span><span class='o'>)</span>

<span class='nv'>impacted_bu</span> <span class='o'>&lt;-</span> <span class='nf'><a href='https://r-spatial.github.io/sf/reference/geos_binary_ops.html'>st_intersection</a></span><span class='o'>(</span><span class='nv'>ner_bu_centroid</span>, <span class='nv'>flood_extent</span><span class='o'>)</span>
<span class='nf'><a href='https://rdrr.io/r/base/nrow.html'>nrow</a></span><span class='o'>(</span><span class='nv'>impacted_bu</span><span class='o'>)</span>

<span class='c'>#&gt; [1] 341</span>
</code></pre>

</div>

We estimated around 341 potentially affected building in the study area.

Limitations
-----------

The goal of this analysis was to show how to conduct a reproducible rapid flood mapping and damage assessment using `R` and Sentintel S2 data. We made several choices during the process and they all have an impact on the final results. For example, other spectral indices could have been used, some authors suggested that Modified NDWI (MDNWI) is more accurate in urban settings and others advocated for the use the Normalized Difference Flood Index for this type of analysis (NDFI). The package `sen2r` is quite flexible and allow to easily add all the above indices. For the impact and damage results, the population grid used in this analysis is based on 2018 population statistics. The analysis would give different results if other population estimate layers such as Worldpop population count grid were used. Finally, <a href="https://en.wikipedia.org/wiki/Synthetic-aperture_radar" target="_blank">SAR imagery</a> can also be used to push the analysis further and estimate for example indicators such as flood water depth for better planing of a response plan.

Session info for this analysis.

<details>
<summary>
Session info
</summary>

<div class="highlight">

<pre class='chroma'><code class='language-r' data-lang='r'><span class='nf'>devtools</span><span class='nf'>::</span><span class='nf'><a href='https://rdrr.io/pkg/sessioninfo/man/session_info.html'>session_info</a></span><span class='o'>(</span><span class='o'>)</span>

<span class='c'>#&gt; ─ Session info ────────────────────────────────────────────────</span>
<span class='c'>#&gt;  setting  value                                      </span>
<span class='c'>#&gt;  version  R version 4.0.3 Patched (2020-10-10 r79320)</span>
<span class='c'>#&gt;  os       Arch Linux                                 </span>
<span class='c'>#&gt;  system   x86_64, linux-gnu                          </span>
<span class='c'>#&gt;  ui       X11                                        </span>
<span class='c'>#&gt;  language (EN)                                       </span>
<span class='c'>#&gt;  collate  en_US.UTF-8                                </span>
<span class='c'>#&gt;  ctype    en_US.UTF-8                                </span>
<span class='c'>#&gt;  tz       UTC                                        </span>
<span class='c'>#&gt;  date     2020-10-12                                 </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; ─ Packages ────────────────────────────────────────────────────</span>
<span class='c'>#&gt;  package     * version     date       lib</span>
<span class='c'>#&gt;  abind       * 1.4-5       2016-07-21 [1]</span>
<span class='c'>#&gt;  assertthat    0.2.1       2019-03-21 [1]</span>
<span class='c'>#&gt;  backports     1.1.10      2020-09-15 [1]</span>
<span class='c'>#&gt;  base64enc     0.1-3       2015-07-28 [1]</span>
<span class='c'>#&gt;  blob          1.2.1       2020-01-20 [1]</span>
<span class='c'>#&gt;  brio          1.1.0       2020-08-31 [1]</span>
<span class='c'>#&gt;  broom         0.7.1       2020-10-02 [1]</span>
<span class='c'>#&gt;  callr         3.5.0       2020-10-08 [1]</span>
<span class='c'>#&gt;  cellranger    1.1.0       2016-07-27 [1]</span>
<span class='c'>#&gt;  class         7.3-17      2020-04-26 [1]</span>
<span class='c'>#&gt;  classInt      0.4-3       2020-04-07 [1]</span>
<span class='c'>#&gt;  cli           2.0.2.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  codetools     0.2-16      2018-12-24 [1]</span>
<span class='c'>#&gt;  colorspace    1.4-1       2019-03-18 [1]</span>
<span class='c'>#&gt;  crayon        1.3.4.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  crosstalk     1.1.0.1     2020-03-13 [1]</span>
<span class='c'>#&gt;  crul          1.0.0       2020-07-30 [1]</span>
<span class='c'>#&gt;  curl          4.3         2019-12-02 [1]</span>
<span class='c'>#&gt;  data.table    1.13.0      2020-07-24 [1]</span>
<span class='c'>#&gt;  DBI           1.1.0       2019-12-15 [1]</span>
<span class='c'>#&gt;  dbplyr        1.4.4       2020-05-27 [1]</span>
<span class='c'>#&gt;  desc          1.2.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  devtools      2.3.2       2020-09-18 [1]</span>
<span class='c'>#&gt;  digest        0.6.25      2020-02-23 [1]</span>
<span class='c'>#&gt;  doParallel    1.0.15      2019-08-02 [1]</span>
<span class='c'>#&gt;  downlit       0.2.0       2020-09-25 [1]</span>
<span class='c'>#&gt;  dplyr       * 1.0.2       2020-08-18 [1]</span>
<span class='c'>#&gt;  e1071         1.7-3       2019-11-26 [1]</span>
<span class='c'>#&gt;  ellipsis      0.3.1.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  evaluate      0.14.1      2020-09-01 [1]</span>
<span class='c'>#&gt;  fansi         0.4.1       2020-01-08 [1]</span>
<span class='c'>#&gt;  forcats     * 0.5.0       2020-03-01 [1]</span>
<span class='c'>#&gt;  foreach       1.5.0       2020-03-30 [1]</span>
<span class='c'>#&gt;  foreign       0.8-80      2020-05-24 [1]</span>
<span class='c'>#&gt;  fs            1.5.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  generics      0.0.2.9000  2020-09-03 [1]</span>
<span class='c'>#&gt;  geojson       0.3.4       2020-06-23 [1]</span>
<span class='c'>#&gt;  geojsonio     0.9.2.93    2020-09-02 [1]</span>
<span class='c'>#&gt;  ggplot2     * 3.3.2       2020-06-19 [1]</span>
<span class='c'>#&gt;  ggthemes    * 4.2.0       2019-05-13 [1]</span>
<span class='c'>#&gt;  glue          1.4.2       2020-08-27 [1]</span>
<span class='c'>#&gt;  gtable        0.3.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  haven         2.3.1       2020-06-01 [1]</span>
<span class='c'>#&gt;  hms           0.5.3       2020-01-08 [1]</span>
<span class='c'>#&gt;  hoardr        0.5.2.93    2020-08-01 [1]</span>
<span class='c'>#&gt;  htmltools     0.5.0       2020-06-16 [1]</span>
<span class='c'>#&gt;  htmlwidgets   1.5.2       2020-10-03 [1]</span>
<span class='c'>#&gt;  httpcode      0.3.0       2020-04-10 [1]</span>
<span class='c'>#&gt;  httr          1.4.2.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  hugodown      0.0.0.9000  2020-10-04 [1]</span>
<span class='c'>#&gt;  iterators     1.0.12      2019-07-26 [1]</span>
<span class='c'>#&gt;  jqr           1.1.0.92    2020-09-02 [1]</span>
<span class='c'>#&gt;  jsonlite      1.7.1       2020-09-07 [1]</span>
<span class='c'>#&gt;  KernSmooth    2.23-17     2020-04-26 [1]</span>
<span class='c'>#&gt;  knitr         1.30        2020-09-22 [1]</span>
<span class='c'>#&gt;  lattice       0.20-41     2020-04-02 [1]</span>
<span class='c'>#&gt;  lazyeval      0.2.2       2019-03-15 [1]</span>
<span class='c'>#&gt;  leafem        0.1.3       2020-07-26 [1]</span>
<span class='c'>#&gt;  leaflet       2.0.3       2019-11-16 [1]</span>
<span class='c'>#&gt;  lifecycle     0.2.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  lubridate     1.7.9       2020-06-08 [1]</span>
<span class='c'>#&gt;  lwgeom        0.2-5       2020-09-01 [1]</span>
<span class='c'>#&gt;  magrittr      1.5.0.9000  2020-09-24 [1]</span>
<span class='c'>#&gt;  maptools      1.0-2       2020-08-24 [1]</span>
<span class='c'>#&gt;  mapview     * 2.9.3       2020-09-08 [1]</span>
<span class='c'>#&gt;  memoise       1.1.0       2017-04-21 [1]</span>
<span class='c'>#&gt;  modelr        0.1.8       2020-05-19 [1]</span>
<span class='c'>#&gt;  munsell       0.5.0       2018-06-12 [1]</span>
<span class='c'>#&gt;  pillar        1.4.6       2020-07-10 [1]</span>
<span class='c'>#&gt;  pkgbuild      1.1.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  pkgconfig     2.0.3       2019-09-22 [1]</span>
<span class='c'>#&gt;  pkgload       1.1.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  png           0.1-7       2013-12-03 [1]</span>
<span class='c'>#&gt;  prettyunits   1.1.1       2020-01-24 [1]</span>
<span class='c'>#&gt;  processx      3.4.4       2020-09-03 [1]</span>
<span class='c'>#&gt;  ps            1.4.0       2020-10-07 [1]</span>
<span class='c'>#&gt;  purrr       * 0.3.4       2020-04-17 [1]</span>
<span class='c'>#&gt;  R6            2.4.1.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  rappdirs      0.3.1       2016-03-28 [1]</span>
<span class='c'>#&gt;  raster        3.3-13      2020-07-17 [1]</span>
<span class='c'>#&gt;  Rcpp          1.0.5       2020-07-06 [1]</span>
<span class='c'>#&gt;  RcppTOML      0.1.6       2019-06-25 [1]</span>
<span class='c'>#&gt;  readr       * 1.4.0       2020-10-05 [1]</span>
<span class='c'>#&gt;  readxl        1.3.1       2019-03-13 [1]</span>
<span class='c'>#&gt;  remotes       2.2.0.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  reprex        0.3.0       2019-05-16 [1]</span>
<span class='c'>#&gt;  rgdal         1.5-17      2020-10-08 [1]</span>
<span class='c'>#&gt;  rgeos         0.5-5       2020-09-07 [1]</span>
<span class='c'>#&gt;  rhdx        * 0.1.0.9000  2020-09-03 [1]</span>
<span class='c'>#&gt;  rlang         0.4.8       2020-10-08 [1]</span>
<span class='c'>#&gt;  rmarkdown     2.4         2020-09-30 [1]</span>
<span class='c'>#&gt;  rprojroot     1.3-2       2018-01-03 [1]</span>
<span class='c'>#&gt;  rstudioapi    0.11        2020-02-07 [1]</span>
<span class='c'>#&gt;  rvest         0.3.6       2020-07-25 [1]</span>
<span class='c'>#&gt;  satellite     1.0.2       2019-12-09 [1]</span>
<span class='c'>#&gt;  scales        1.1.1.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  sen2r       * 1.3.8       2020-08-26 [1]</span>
<span class='c'>#&gt;  sessioninfo   1.1.1.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  sf          * 0.9-7       2020-10-10 [1]</span>
<span class='c'>#&gt;  snapbox     * 0.2.1       2020-09-09 [1]</span>
<span class='c'>#&gt;  sp            1.4-4       2020-10-07 [1]</span>
<span class='c'>#&gt;  stars       * 0.4-4       2020-10-10 [1]</span>
<span class='c'>#&gt;  stringi       1.5.3       2020-09-09 [1]</span>
<span class='c'>#&gt;  stringr     * 1.4.0       2019-02-10 [1]</span>
<span class='c'>#&gt;  stylebox      0.1.0       2020-09-09 [1]</span>
<span class='c'>#&gt;  testthat      2.99.0.9000 2020-09-01 [1]</span>
<span class='c'>#&gt;  tibble      * 3.0.3       2020-07-10 [1]</span>
<span class='c'>#&gt;  tidyr       * 1.1.2       2020-08-27 [1]</span>
<span class='c'>#&gt;  tidyselect    1.1.0       2020-05-11 [1]</span>
<span class='c'>#&gt;  tidyverse   * 1.3.0       2019-11-21 [1]</span>
<span class='c'>#&gt;  units       * 0.6-7       2020-06-13 [1]</span>
<span class='c'>#&gt;  usethis       1.6.3       2020-09-17 [1]</span>
<span class='c'>#&gt;  V8            3.2.0       2020-06-19 [1]</span>
<span class='c'>#&gt;  vctrs         0.3.4.9000  2020-09-01 [1]</span>
<span class='c'>#&gt;  webshot       0.5.2       2019-11-22 [1]</span>
<span class='c'>#&gt;  withr         2.3.0       2020-09-22 [1]</span>
<span class='c'>#&gt;  xfun          0.18        2020-09-29 [1]</span>
<span class='c'>#&gt;  XML           3.99-0.5    2020-07-23 [1]</span>
<span class='c'>#&gt;  xml2          1.3.2.9001  2020-09-01 [1]</span>
<span class='c'>#&gt;  yaml          2.2.1       2020-02-01 [1]</span>
<span class='c'>#&gt;  source                                </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  Github (r-lib/hugodown@18911fc)       </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  Github (r-spatial/lwgeom@99d07a7)     </span>
<span class='c'>#&gt;  Github (tidyverse/magrittr@0221e18)   </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  Github (r-spatial/mapview@3b0810b)    </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  Github (r-spatial/sf@5caaca7)         </span>
<span class='c'>#&gt;  Github (anthonynorth/snapbox@ba3141b) </span>
<span class='c'>#&gt;  CRAN (R 4.0.3)                        </span>
<span class='c'>#&gt;  Github (r-spatial/stars@7f8ef33)      </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  Github (anthonynorth/stylebox@500218e)</span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt;  local                                 </span>
<span class='c'>#&gt;  CRAN (R 4.0.2)                        </span>
<span class='c'>#&gt; </span>
<span class='c'>#&gt; [1] /usr/lib/R/library</span>
</code></pre>

</div>

</details>

